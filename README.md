## Python program to calculate the angle between hour hand and minute hand

This program takes the input as time in 12h/24h time format.

It shows the angle between the minute and hour hands of an analogue clock based on the input time.

You must have Python version 3.x.x installed on your machine in order to execute this code.

To run this code execute the following command

```bash
    python3 clock.py

```

There will be no output on the screen. Just enter the time in HH:mm format and press enter

The program will display one single line of output which will be the angle between the minute and hour hand
