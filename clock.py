h, m = map(int, input().split(':'))  # To split the hours and minutes based on ':' as delimiter
h = h % 12  # If the time is entered in 24h format we take its modulus
m = m % 60  # If entered minute is more than 60
d_h = 30 * int(h)  # Angle made by hour hand with Y - axis
d_m = 6 * m  # Angle made by minute hand with Y - axis
offset = (int(m) / 60) * 30  # Movement of hour hand based on the minutes passed
d_h += offset  # Adding the offset calculated in the above step to the angle of hour hand
print(abs(d_m - d_h) % 180)  # Answer is calculated by taking the absolute difference between the angles of hour and minute hands
